# OpenML dataset: Traffic-counting-using-cameras

https://www.openml.org/d/43768

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
These data come from a camera that is part of the Telraam device which makes counting cameras available to interested citizens. 
https://www.telraam.net/fr/what-is-telraam
This camera is located, place gnral de gaulle, Paris (mouans sartoux, France)
https://www.telraam.net/fr/location/9000000411
What exactly does the Telraam measure ?
Telraam counts pedestrians, cyclists, cars and freight/heavy vehicles. This is done using images taken by the device camera and the analysis performed by the Raspberry Pi (a small computer on which the device is based). The analysis simply uses the size and speed of the passing object.
Each hour the camera records the following data:

Percentage of camera activity
Counting of pedestrians, cars, bicycles, trucks (total, left and right of the street)
Histogram of car speeds for the intervals [0-10 [[10-20 [[20- 30 [.. [70 and more [

Why a percentage of camera activity ?
Telraam does not count when it is dark.
This camera has constraints, because to perform the counts, it does image recognition:

The camera is not active at night
The camera is not active 100 of the time in daylight, therefore the percentage of activity is indicated. When the camera is partially active, the counts are prorated using this percentage to estimate the activity in the observed hour.
The camera may be out of service for a period

Content
The data has been flatten in a csv file.
The original data (json) can be retrieved in real time here with a POST : https://telraam-api.net/v0/reports/9000000411
The API Documentation:  https://telraam.zendesk.com/hc/en-us/articles/360027325572-Want-more-data-Telraam-API
Columns
['time','id','timezone','pctup','pedestrian','bike','car','lorry','pedestrianlft','bikelft','carlft','lorrylft','pedestrianrgt','bikergt','carrgt','lorryrgt','carspeed00','carspeed10','carspeed20','carspeed30','carspeed40','carspeed50','carspeed60','carspeed_70']
Inspiration
Time series forecasting : predict traffic flow

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43768) of an [OpenML dataset](https://www.openml.org/d/43768). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43768/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43768/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43768/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

